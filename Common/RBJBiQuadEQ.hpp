//
//  RBJBiQuadEQ.hpp
//  MyChorusPlugin_VST
//
//  Created by on 31/12/2022.
//

#ifndef RBJBiQuadEQ_hpp
#define RBJBiQuadEQ_hpp

#include <stdio.h>
#include <math.h>

/* Class abstracting a biquad IIR filter from the RBJ cookbook. */
class RBJBiQuadEQ
{
    public :
    RBJBiQuadEQ();
    ~RBJBiQuadEQ();
    
    void setSampleRate(double sampleRate);
    void setCutOffFreq(double cutoffFreq);
    void setQFactor(double qFactor);
    double calculateLPFCoeff();
    double calculateHPFCoeff();
    double calculateBPFCoeff();
    double getfilteredOutput(double inputSample);
    void normaliseCoeff();
    void resetCoeff();
    
private:
    double f0, Q;
    double b0,b1,b2,a1,a2,z1,z2;
    double fs, w0, alpha;
    double a0;
    double filteredOut = 0.0;
};

#endif /* RBJBiQuadEQ_hpp */
