//
//  RBJBiQuadEQ.cpp
//  MyChorusPlugin_VST
//
//  Created by on 31/12/2022.
//

#include "RBJBiQuadEQ.hpp"

// Destructor
RBJBiQuadEQ::RBJBiQuadEQ()
{
    
}

// Destructor
RBJBiQuadEQ::~RBJBiQuadEQ()
{
    
}

// Set sample rate for calculations.
void RBJBiQuadEQ::setSampleRate(double sampleRate)
{
    fs = sampleRate;
}

// Set cutoff frequency.
void RBJBiQuadEQ::setCutOffFreq(double cutoffFreq)
{
    f0 = cutoffFreq;
    w0 = 2.0*M_PI*f0/fs;
}

// Set Q factor.
void RBJBiQuadEQ::setQFactor(double qFactor)
{
    Q = qFactor;
    alpha = sin(w0)/(2.0*Q);
}

// Calculate coefficients for a LPF.
double RBJBiQuadEQ::calculateLPFCoeff()
{
    a0 = 1.0 + alpha;
    b0 = (1.0 - cos(w0)) / 2.0;
    b1 = 1.0 - cos(w0);
    b2 = (1.0 - cos(w0)) / 2.0;
    a1 = -2.0 * cos(w0);
    a2 = 1.0 - alpha;             
    
    normaliseCoeff();
}

// Calculate coefficients for a HPF.
double RBJBiQuadEQ::calculateHPFCoeff()
{
    a0 = 1.0 + alpha;
    b0 = (1.0 + cos(w0)) / 2.0;
    b1 = -(1.0 + cos(w0));
    b2 = (1.0 + cos(w0)) / 2.0;
    a1 = -2.0 * cos(w0);
    a2 = 1.0 - alpha;
    
    normaliseCoeff();
}

// Calculated filtered output sample from input sample.
double RBJBiQuadEQ::getfilteredOutput(double inputSample)
{
    filteredOut = b0*inputSample + z1;
    z1 = b1*inputSample - a1*filteredOut + z2;
    z2 = b2*inputSample - a2*filteredOut;
    
    return filteredOut;
}

// Normalise coefficients.
void RBJBiQuadEQ::normaliseCoeff()
{
    b0/=a0;
    b1/=a0;
    b2/=a0;
    a1/=a0;
    a2/=a0;
}

// Reset coefficients.
void RBJBiQuadEQ::resetCoeff()
{
    z1 = z2 = 0.0;
}

