//
//  Delay.cpp
//  MyChorusPlugin_VST
//
//  Created by on 22/12/2022.
//

#include "Delay.hpp"

// Constructor
Delay::Delay()
{
    maxDelay = 20000;                   // Initialise maximum delay in samples for safety before setSampleRate()
    rptr1 = rptr2 = wptr = 0;           // Initialise integer delay pointer positions
    frac = delTapPoint = 0.0;           // Initialise fractional delay values in samples
    delLine = std::make_unique<double[]>(maxDelay);  // Dynamically allocate and initialise delay buffer
    previousInterp = 0.0;               // Initialise previous sample value for allpass interpolation calculation
}

// Destructor
Delay::~Delay()
{
//    delete [] delLine;                  // Deallocate delay buffer
}

// Set sample rate.
void Delay::setSampleRate(double sampleRate)
{
    maxDelay = sampleRate;              // Set maximum delay buffer size to based on the host DAW's sample rate
}

// Reset pointers and buffer.
void Delay::reset()
{
    rptr1 = rptr2 = wptr = 0;                        // Reset pointer positions to 0 between audio streams/clips in the DAW
    delLine = std::make_unique<double[]>(maxDelay);  // Dynamically allocate the delay buffer based on the current sample rate
}

// Set delay time.
void Delay::setDelayTime(double mDelay)
{
    delTapPoint = mDelay;               // Store incoming delay time value in samples.
}

// Calculate pointer positions.
void Delay::calculatePointerPositions()
{
    rptr1 = wptr - (int)delTapPoint;    // Calculate 1st read pointer position
    rptr2 = rptr1 - 1;                  // Calculate 2nd read pointer position, one sample away
    
//  Wrap around the two read pointers if they've underflowed
    if (rptr1 < 0)
    {
        rptr1 += maxDelay;
    }
    
    if(rptr2 < 0)
    {
        rptr2 += maxDelay;
    }
   
    frac = delTapPoint - (int)delTapPoint;  // Store the fractional part of the delay value in samples
}

// Return interpolated delayed sample.
double Delay::getInterpDelayedSample()
{
    double alphaCoeff = (1 - frac)/(1 + frac);    // All pass coefficient alpha
    
//  Linear interpolation calculation
//  double delayedSample = delLine[rptr1] + (delLine[rptr2] - delLine[rptr1]) * frac;
    
//  First order all pass interpolation calculation  y [ n ] = α · x [ n ] + x [ n − 1] − α · y [ n − 1]
    double delayedSample = alphaCoeff * delLine[rptr1] + delLine[rptr2] - alphaCoeff * previousInterp;
    
    previousInterp = delayedSample;      // Store current interpolated sample for the next interpolated sample calculation
    
    return delayedSample;
}

// Write incoming sample to the delay line.
void Delay::writeToDelay(double sampleToFeed)
{
    delLine[wptr] = sampleToFeed;       // Write the passed sample in to the delay line buffer
    wptr++;                             // Progress the write pointer to the next position
    
//  Wrap around the write pointer if it's overflowed
    if(wptr>=maxDelay)
    {
        wptr = 0;
    }
}
