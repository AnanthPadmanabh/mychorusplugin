//
//  Delay.hpp
//  MyChorusPlugin_VST
//
//  Created by on 22/12/2022.
//

#ifndef Delay_hpp
#define Delay_hpp

#include <stdio.h>
#include <memory>
#include <utility>

/* Class abstracting a single tap delay line. */
class Delay
{
public:
    Delay();
    ~Delay();
    void setSampleRate(double sampleRate);
    void reset();
    void setDelayTime(double mDelay);
    void calculatePointerPositions();
    double getInterpDelayedSample();
    void writeToDelay(double sampleToFeed);
    
private:
    int maxDelay;
    int rptr1, rptr2, wptr;
    double frac, delTapPoint;
    std::unique_ptr<double[]> delLine;
//    double *delLine;
    double previousInterp;
    
};

#endif /* Delay_hpp */
