//
//  DeZipper.cpp
//  Week3_Stereo_Width_VST
//
//  Created by on 20/10/2022.
//

#include "DeZipper.hpp"

// Constructor
DeZipper::DeZipper()
{
    m_DZMM = 0.0;
    m_DZFB = 0.999;
    m_DZFF = 1 - m_DZFB;
}

// Return dezipped value.
double DeZipper::smooth(double sample)
{
    double temp = m_DZFF * sample + m_DZFB * m_DZMM;
    m_DZMM = temp;
    
    return temp;
}
