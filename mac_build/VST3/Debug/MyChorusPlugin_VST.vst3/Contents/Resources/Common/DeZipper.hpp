//
//  DeZipper.hpp
//  Week3_Stereo_Width_VST
//
//  Created by on 20/10/2022.
//

#ifndef DeZipper_hpp
#define DeZipper_hpp

/* Class for a one pole de-zip filter for control parameters. */
class DeZipper
{
    
public:
    DeZipper();
    double smooth(double sample);
    
private:
    double m_DZMM;
    double m_DZFB;
    double m_DZFF;
    
};

#endif /* DeZipper_hpp */
