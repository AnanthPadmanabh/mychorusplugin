//
//  WaveShaper.cpp
//  MyChorusPlugin_VST
//
//  Created by on 31/12/2022.
//

#include "WaveShaper.hpp"

// Constructor
WaveShaper::WaveShaper()
{
    k = 0.1;
}

// Destructor
WaveShaper::~WaveShaper()
{
    
}

// Functions for diff waveshaping algorithms

double WaveShaper::getHTanWaveshape(double x)
{
    double y = tanh(k*x)/tanh(k);
    return y;
}

double WaveShaper::getATanWaveshape(double x)
{
    double y = atan(k*x)/atan(k);
    return y;
}

// Set overdrive amount.
void WaveShaper::setOverdrive(double overDrive)
{
    k = overDrive;
}

