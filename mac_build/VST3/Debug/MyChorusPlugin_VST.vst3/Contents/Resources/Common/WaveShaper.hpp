//
//  WaveShaper.hpp
//  MyChorusPlugin_VST
//
//  Created by on 31/12/2022.
//

#ifndef WaveShaper_hpp
#define WaveShaper_hpp

#include <stdio.h>
#include <math.h>

/* Class for various waveshaping functions as shown in Designing Audio Effects Plugins in C++ by Will Pirkle. */
class WaveShaper
{
public:
    
    WaveShaper();
    ~WaveShaper();
    double getHTanWaveshape(double x);
    double getATanWaveshape(double x);
    double getArrayaWaveshape(double x);
    double getFuzzExpWaveshape(double x);
    double getAsymmetricWaveshape(double x);
    void setOverdrive(double saturation);
    
private:
    double k;
};

#endif /* WaveShaper_hpp */
