#!/bin/sh
set -e
if test "$CONFIGURATION" = "Debug"; then :
  cd /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build
  make -f /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/CMakeScripts/ReRunCMake.make
fi
if test "$CONFIGURATION" = "Release"; then :
  cd /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build
  make -f /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/CMakeScripts/ReRunCMake.make
fi
if test "$CONFIGURATION" = "MinSizeRel"; then :
  cd /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build
  make -f /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/CMakeScripts/ReRunCMake.make
fi
if test "$CONFIGURATION" = "RelWithDebInfo"; then :
  cd /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build
  make -f /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/CMakeScripts/ReRunCMake.make
fi

