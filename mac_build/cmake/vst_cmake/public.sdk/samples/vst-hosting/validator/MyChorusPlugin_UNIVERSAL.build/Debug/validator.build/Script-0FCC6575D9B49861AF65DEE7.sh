#!/bin/sh
set -e
if test "$CONFIGURATION" = "Debug"; then :
  cd /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/cmake/vst_cmake/public.sdk/samples/vst-hosting/validator
  /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/bin/Debug/validator -selftest
fi
if test "$CONFIGURATION" = "Release"; then :
  cd /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/cmake/vst_cmake/public.sdk/samples/vst-hosting/validator
  /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/bin/Release/validator -selftest
fi

