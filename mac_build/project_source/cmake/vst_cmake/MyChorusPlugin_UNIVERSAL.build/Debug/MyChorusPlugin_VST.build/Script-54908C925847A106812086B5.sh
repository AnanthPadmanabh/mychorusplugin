#!/bin/sh
set -e
if test "$CONFIGURATION" = "Debug"; then :
  cd /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/bin
  /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/bin/Debug/validator /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/VST3/Debug/MyChorusPlugin_VST.vst3 
  cd /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/project_source/cmake/vst_cmake
  mkdir -p /Users/ananthapadmanabh/Library/Audio/Plug-Ins/VST3
  ln -svfF /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/VST3/Debug/MyChorusPlugin_VST.vst3 /Users/ananthapadmanabh/Library/Audio/Plug-Ins/VST3
fi
if test "$CONFIGURATION" = "Release"; then :
  cd /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/bin
  /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/bin/Release/validator  /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/VST3/Release/MyChorusPlugin_VST.vst3
  cd /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/project_source/cmake/vst_cmake
  mkdir -p /Users/ananthapadmanabh/Library/Audio/Plug-Ins/VST3
  ln -svfF /Users/ananthapadmanabh/Desktop/Software/ALL_SDK/myprojects/MyChorusPlugin/mac_build/VST3/Release/MyChorusPlugin_VST.vst3 /Users/ananthapadmanabh/Library/Audio/Plug-Ins/VST3
fi

