// -----------------------------------------------------------------------------
//    ASPiK Plugin Kernel File:  plugincore.cpp
//
/**
    \file   plugincore.cpp
    \author Will Pirkle
    \date   17-September-2018
    \brief  Implementation file for PluginCore object
    		- http://www.aspikplugins.com
    		- http://www.willpirkle.com
*/
// -----------------------------------------------------------------------------
#include "plugincore.h"
#include "plugindescription.h"

/**
\brief PluginCore constructor is launching pad for object initialization

Operations:
- initialize the plugin description (strings, codes, numbers, see initPluginDescriptors())
- setup the plugin's audio I/O channel support
- create the PluginParameter objects that represent the plugin parameters (see FX book if needed)
- create the presets
*/
PluginCore::PluginCore()
{
    // --- describe the plugin; call the helper to init the static parts you setup in plugindescription.h
    initPluginDescriptors();

    // --- default I/O combinations
	// --- for FX plugins
	if (getPluginType() == kFXPlugin)
	{
//		addSupportedIOCombination({ kCFMono, kCFMono });
//		addSupportedIOCombination({ kCFMono, kCFStereo });
		addSupportedIOCombination({ kCFStereo, kCFStereo });
	}
	else // --- synth plugins have no input, only output
	{
		addSupportedIOCombination({ kCFNone, kCFMono });
		addSupportedIOCombination({ kCFNone, kCFStereo });
	}

	// --- for sidechaining, we support mono and stereo inputs; auxOutputs reserved for future use
	addSupportedAuxIOCombination({ kCFMono, kCFNone });
	addSupportedAuxIOCombination({ kCFStereo, kCFNone });

	// --- create the parameters
    initPluginParameters();

    // --- create the presets
    initPluginPresets();
}

/**
\brief initialize object for a new run of audio; called just before audio streams

Operation:
- store sample rate and bit depth on audioProcDescriptor - this information is globally available to all core functions
- reset your member objects here

\param resetInfo structure of information about current audio format

\return true if operation succeeds, false otherwise
*/
bool PluginCore::reset(ResetInfo& resetInfo)
{
    // --- save for audio processing
    audioProcDescriptor.sampleRate = resetInfo.sampleRate;
    audioProcDescriptor.bitDepth = resetInfo.bitDepth;
    
    // Reset voices for both channels and set sample rate.
    for(int i = 0; i < VOICE_COUNT; i++)
    {
        leftChannelVoice[i].reset();
        leftChannelVoice[i].setSampleRate(getSampleRate());
        rightChannelVoice[i].reset();
        rightChannelVoice[i].setSampleRate(getSampleRate());
    }
    // --- other reset inits
    return PluginBase::reset(resetInfo);
}

/**
\brief one-time initialize function called after object creation and before the first reset( ) call

Operation:
- saves structure for the plugin to use; you can also load WAV files or state information here
*/
bool PluginCore::initialize(PluginInfo& pluginInfo)
{
	// --- add one-time init stuff here
    
    leftChannelVoice = std::make_unique<Delay[]>(VOICE_COUNT);
    rightChannelVoice = std::make_unique<Delay[]>(VOICE_COUNT);
    
    // Initialise sample rate for the voices to calculate max delay.
    for(int i = 0; i < VOICE_COUNT; i++)
    {
        leftChannelVoice[i].setSampleRate(getSampleRate());
        rightChannelVoice[i].setSampleRate(getSampleRate());
    }
    
    // Reset feedback HPF coefficients and set sample rate.
    feedbackFilter.resetCoeff();
    feedbackFilter.setSampleRate(getSampleRate());

    // Reset pre distortion filter coefficients and set sample rate.
    preDistortionFilter.resetCoeff();
    preDistortionFilter.setSampleRate(getSampleRate());
	return true;
}

/**
\brief do anything needed prior to arrival of audio buffers

Operation:
- syncInBoundVariables when preProcessAudioBuffers is called, it is *guaranteed* that all GUI control change information
  has been applied to plugin parameters; this binds parameter changes to your underlying variables
- NOTE: postUpdatePluginParameter( ) will be called for all bound variables that are acutally updated; if you need to process
  them individually, do so in that function
- use this function to bulk-transfer the bound variable data into your plugin's member object variables

\param processInfo structure of information about *buffer* processing

\return true if operation succeeds, false otherwise
*/
bool PluginCore::preProcessAudioBuffers(ProcessBufferInfo& processInfo)
{
    // --- sync internal variables to GUI parameters; you can also do this manually if you don't
    //     want to use the auto-variable-binding
    syncInBoundVariables();

    return true;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	--- FRAME PROCESSING FUNCTION --- //
/*
	This is where you do your DSP processing on a PER-FRAME basis; this means that you are
	processing one sample per channel at a time, and is the default mechanism in ASPiK.

	You will get better performance is you process buffers or blocks instead, HOWEVER for
	some kinds of processing (e.g. ping pong delay) you must process on a per-sample
	basis anyway. This is also easier to understand for most newbies.

	NOTE:
	You can enable and disable frame/buffer procssing in the constructor of this object:

	// --- to process audio frames call:
	processAudioByFrames();

	// --- to process complete DAW buffers call:
	processAudioByBlocks(WANT_WHOLE_BUFFER);

	// --- to process sub-blocks of the incoming DAW buffer in 64 sample blocks call:
	processAudioByBlocks(DEFAULT_AUDIO_BLOCK_SIZE);

	// --- to process sub-blocks of size N, call:
	processAudioByBlocks(N);
*/
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

/**
 \brief frame-processing method
 
 Operation:
 - decode the plugin type - for synth plugins, fill in the rendering code; for FX plugins, delete the if(synth) portion and add your processing code
 - note that MIDI events are fired for each sample interval so that MIDI is tightly sunk with audio
 - doSampleAccurateParameterUpdates will perform per-sample interval smoothing
 
 \param processFrameInfo structure of information about *frame* processing
 
 \return true if operation succeeds, false otherwise
 */
bool PluginCore::processAudioFrame(ProcessFrameInfo& processFrameInfo)
{
    // --- fire any MIDI events for this sample interval
    processFrameInfo.midiEventQueue->fireMidiEvents(processFrameInfo.currentFrame);

	// --- do per-frame smoothing
	doParameterSmoothing();

	// --- call your GUI update/cooking function here, now that smoothing has occurred
	//
	//     NOTE:
	//     updateParameters is the name used in Will Pirkle's books for the GUI update function
	//     you may name it what you like - this is where GUI control values are cooked
	//     for the DSP algorithm at hand
	// updateParameters();

    // --- decode the channelIOConfiguration and process accordingly
    //
    // --- Stereo-In/Stereo-Out
    
    double inL = processFrameInfo.audioInputFrame[0];                 // Store input sample for L channel
    double inR = processFrameInfo.audioInputFrame[1];                 // Store input sample for R channel
    double delayedInL = 0.0, delayedInR = 0.0, outL = inL, outR = inR;// Initialise variables for delayed and final output for both channels
    
    // Store dezipped values of gui control parameters
    dzRateVal = dzModRate.smooth(modRate);
    dzDepthLAmt = dzModDepthL.smooth(cookedDepthL);
    dzDepthRAmt = dzModDepthR.smooth(cookedDepthR);
    dzDelTimeVal = dzDelTime.smooth(cookedDelay);
    dzFeedbackAmt = dzDelFeedback.smooth(cookedFeedback);
    dzDryAmount = dzModMix.smooth(dryAmount);
    dzWetAmount = 1.0 - dzDryAmount;
    dzPhaseOffsetVal = dzPhaseOffset.smooth(cookedPhaseOffset);
    dzVolumeLin = dzVolume.smooth(volumeLin);
    
    // Declare arrays to store modulator signal output for each of the voices
    double lfoModLVoice[VOICE_COUNT], lfoModRVoice[VOICE_COUNT];
    
    // Count up and initialise the arrays such that each voice's modulator signal is phase shifted incrementally
    for(int i = 0; i < (voiceCount); i++)
    {
        lfoModLVoice[i] = sin(2 * M_PI * (modRate) * sample/getSampleRate() + (M_PI * i/voiceCount));
        lfoModRVoice[i] = sin(2 * M_PI * (modRate) * sample/getSampleRate() - (M_PI * dzPhaseOffsetVal) + (M_PI * i/(voiceCount)));
    }
    
    // Waveform type selector
    switch(waveformType)
    {
        // Sine modulator wave
        case enumToInt(WaveformType::Sine):
            break;
            
        // Asymmetric modulator wave
        case enumToInt(WaveformType::Complex):
        for(int i = 0; i < (voiceCount); i++)
        {
//         lfoModLVoice[i] = 2.5 * atan(0.9 *(lfoModLVoice[i]))+ 2.5 * sqrt(1 -(0.9*lfoModLVoice[i])) - 2.5;
//         lfoModRVoice[i] = 2.5 * atan(0.9 *(lfoModRVoice[i]))+ 2.5 * sqrt(1 -(0.9*lfoModRVoice[i])) - 2.5;
            lfoModLVoice[i] = (2 * ((modRate*sample/getSampleRate())*(1/modRate))*modRate)-1;
            lfoModRVoice[i] = (2 * ((modRate*sample/getSampleRate())*(1/modRate))*modRate)-1;
        }
        break;

        default: ;
    }

    sample++; // Increment sample counter
    
    // Arrays to store delayed L and R samples for all voices
    double delayedInLVoice[VOICE_COUNT], delayedInRVoice[VOICE_COUNT];
    
    // Variables to store filtered input signals for L and R channels
    double lpfInL = 0.0;
    double lpfInR = 0.0;
    
    // Variables to store sum of delayed voices for L and R channels
    delayedInL = 0.0; delayedInR = 0.0;
    
    for(int i = 0; i < voiceCount; i++)
    {
        // Store the modulated delay time value for each voice for L and R channels
        modDelayTimeLVoice[i] = dzDelTimeVal + dzDelTimeVal * dzDepthLAmt * lfoModLVoice[i];
        modDelayTimeRVoice[i] = dzDelTimeVal + dzDelTimeVal * dzDepthRAmt * lfoModRVoice[i];
        
        // Set delay time for each voice for L and R channels, using the modulation signal
        leftChannelVoice[i].setDelayTime(modDelayTimeLVoice[i]);
        rightChannelVoice[i].setDelayTime(modDelayTimeRVoice[i]);
        
        // Calculate read pointer positions for each voice for L and R channels
        leftChannelVoice[i].calculatePointerPositions();
        rightChannelVoice[i].calculatePointerPositions();
        
        // Store unique delay taps for each voice for L and R channels
        delayedInLVoice[i] = leftChannelVoice[i].getInterpDelayedSample();
        delayedInRVoice[i] = rightChannelVoice[i].getInterpDelayedSample();
        
        // Switch between series and cross feedback terms for L and R channels
        feedbackTermL = crossfeedback ? delayedInLVoice[i] : delayedInRVoice[i];
        feedbackTermR = crossfeedback ? delayedInRVoice[i] : delayedInLVoice[i];
        
        // Store filtered L and R input samples
        lpfInL = preDistortionFilter.getfilteredOutput(inL);
        lpfInR = preDistortionFilter.getfilteredOutput(inR);
        
        // Write the input and the feedback signals to the L and R delay channels where input is filtered and distorted
        leftChannelVoice[i].writeToDelay(preChorusDistortion.getATanWaveshape(lpfInL) - feedbackFilter.getfilteredOutput(dzFeedbackAmt * feedbackTermL));
        rightChannelVoice[i].writeToDelay(preChorusDistortion.getATanWaveshape(lpfInR) - feedbackFilter.getfilteredOutput(dzFeedbackAmt * feedbackTermR));
        
        // Scaling and summing the amplitude of each voice for L and R channels
        delayedInL+= delayedInLVoice[i] * 1/3;
        delayedInR+= delayedInRVoice[i] * 1/3;
    }

    // If the power switch is on then and pass through the processed samples to the output as well as to the VU meter.
    if(!effectBypass)
    {
        outL = (inL * dzDryAmount + delayedInL * dzWetAmount) * dzVolumeLin;
        outR = (inR * dzDryAmount + delayedInR * dzWetAmount) * dzVolumeLin;
        vuL = outL;
        vuR = outR;
    }
    // If the power switch is off then pass through the unprocessed input samples to the output and don't feed the VU meter.
    else
    {
        outL = inL;
        outR = inR;
        vuL = 0.0;
        vuR = 0.0;
    }
    // Feed the processed/unprocessed output samples back to the host DAW.
    processFrameInfo.audioOutputFrame[0] = outL;
    processFrameInfo.audioOutputFrame[1] = outR;
    
    return true; /// processed

}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	--- BLOCK/BUFFER PRE-PROCESSING FUNCTION --- //
//      Only used when BLOCKS or BUFFERS are processed
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

/**
\brief pre-process the audio block

Operation:
- fire MIDI events for the audio block; see processMIDIEvent( ) for the code that loads
  the vector on the ProcessBlockInfo structure

\param IMidiEventQueue ASPIK event queue of MIDI events for the entire buffer; this
       function only fires the MIDI events for this audio block

\return true if operation succeeds, false otherwise
*/
bool PluginCore::preProcessAudioBlock(IMidiEventQueue* midiEventQueue)
{
	// --- pre-process the block
	processBlockInfo.clearMidiEvents();

	// --- sample accurate parameter updates
	for (uint32_t sample = processBlockInfo.blockStartIndex;
		sample < processBlockInfo.blockStartIndex + processBlockInfo.blockSize;
		sample++)
	{
		// --- the MIDI handler will load up the vector in processBlockInfo
		if (midiEventQueue)
			midiEventQueue->fireMidiEvents(sample);
	}

	// --- this will do parameter smoothing ONLY ONCE AT THE TOP OF THE BLOCK PROCESSING
	//
	// --- to perform per-sample parameter smoothing, move this line of code, AND your updating
	//     functions (see updateParameters( ) in comment below) into the for( ) loop above
	//     NOTE: smoothing only once per block usually SAVES CPU cycles
	//           smoothing once per sample period usually EATS CPU cycles, potentially unnecessarily
	doParameterSmoothing();

	// --- call your GUI update/cooking function here, now that smoothing has occurred
	//
	//     NOTE:
	//     updateParameters is the name used in Will Pirkle's books for the GUI update function
	//     you may name it what you like - this is where GUI control values are cooked
	//     for the DSP algorithm at hand
	//     NOTE: updating (cooking) only once per block usually SAVES CPU cycles
	//           updating (cooking) once per sample period usually EATS CPU cycles, potentially unnecessarily
	// updateParameters();

	return true;
}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	--- BLOCK/BUFFER PROCESSING FUNCTION --- //
/*
This is where you do your DSP processing on a PER-BLOCK basis; this means that you are
processing entire blocks at a time -- each audio channel has its own block to process.

A BUFFER is simply a single block of audio that is the size of the incoming buffer from the
DAW. A BLOCK is a sub-block portion of that buffer.

In the event that the incoming buffer is SMALLER than your requested audio block, the
entire buffer will be sent to this block processing function. This is also true when your
block size is not a divisor of the actual incoming buffer, OR when an incoming buffer
is partially filled (which is rare, but may happen under certain circumstances), resulting
in a "partial block" of data that is smaller than your requested block size.

NOTE:
You can enable and disable frame/buffer procssing in the constructor of this object:

// --- to process audio frames call:
processAudioByFrames();

// --- to process complete DAW buffers call:
processAudioByBlocks(WANT_WHOLE_BUFFER);

// --- to process sub-blocks of the incoming DAW buffer in 64 sample blocks call:
processAudioByBlocks(DEFAULT_AUDIO_BLOCK_SIZE);

// --- to process sub-blocks of size N, call:
processAudioByBlocks(N);
*/
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

/**
\brief block or buffer-processing method

Operation:
- process one block of audio data; see example functions for template code
- renderSynthSilence: render a block of 0.0 values (synth, silence when no notes are rendered)
- renderFXPassThrough: pass audio from input to output (FX)

\param processBlockInfo structure of information about *block* processing

\return true if operation succeeds, false otherwise
*/
bool PluginCore::processAudioBlock(ProcessBlockInfo& processBlockInfo)
{
	// --- FX or Synth Render
	//     call your block processing function here
	// --- Synth
	if (getPluginType() == kSynthPlugin)
		renderSynthSilence(processBlockInfo);

	// --- or FX
	else if (getPluginType() == kFXPlugin)
		renderFXPassThrough(processBlockInfo);

	return true;
}


/**
\brief
renders a block of silence (all 0.0 values) as an example
your synth code would render the synth using the MIDI messages and output buffers

Operation:
- process all MIDI events for the block
- perform render into block's audio buffers

\param blockInfo structure of information about *block* processing
\return true if operation succeeds, false otherwise
*/
bool PluginCore::renderSynthSilence(ProcessBlockInfo& blockInfo)
{
	// --- process all MIDI events in this block (same as SynthLab)
	uint32_t midiEvents = blockInfo.getMidiEventCount();
	for (uint32_t i = 0; i < midiEvents; i++)
	{
		// --- get the event
		midiEvent event = *blockInfo.getMidiEvent(i);

		// --- do something with it...
		// myMIDIMessageHandler(event); // <-- you write this
	}

	// --- render a block of audio; here it is silence but in your synth
	//     it will likely be dependent on the MIDI processing you just did above
	for (uint32_t sample = blockInfo.blockStartIndex, i = 0;
		 sample < blockInfo.blockStartIndex + blockInfo.blockSize;
		 sample++, i++)
	{
		// --- write outputs
		for (uint32_t channel = 0; channel < blockInfo.numAudioOutChannels; channel++)
		{
			// --- silence (or, your synthesized block of samples)
			blockInfo.outputs[channel][sample] = 0.0;
		}
	}
	return true;
}

/**
\brief
Renders pass-through code as an example; replace with meaningful DSP for audio goodness

Operation:
- loop over samples in block
- write inputs to outputs, per channel basis

\param blockInfo structure of information about *block* processing
\return true if operation succeeds, false otherwise
*/
bool PluginCore::renderFXPassThrough(ProcessBlockInfo& blockInfo)
{
	// --- block processing -- write to outputs
	for (uint32_t sample = blockInfo.blockStartIndex, i = 0;
		sample < blockInfo.blockStartIndex + blockInfo.blockSize;
		sample++, i++)
	{
		// --- handles multiple channels, but up to you for bookkeeping
		for (uint32_t channel = 0; channel < blockInfo.numAudioOutChannels; channel++)
		{
			// --- pass through code, or your processed FX version
			blockInfo.outputs[channel][sample] = blockInfo.inputs[channel][sample];
		}
	}
	return true;
}


/**
\brief do anything needed prior to arrival of audio buffers

Operation:
- updateOutBoundVariables sends metering data to the GUI meters

\param processInfo structure of information about *buffer* processing

\return true if operation succeeds, false otherwise
*/
bool PluginCore::postProcessAudioBuffers(ProcessBufferInfo& processInfo)
{
	// --- update outbound variables; currently this is meter data only, but could be extended
	//     in the future
	updateOutBoundVariables();

    return true;
}

/**
\brief update the PluginParameter's value based on GUI control, preset, or data smoothing (thread-safe)

Operation:
- update the parameter's value (with smoothing this initiates another smoothing process)
- call postUpdatePluginParameter to do any further processing

\param controlID the control ID value of the parameter being updated
\param controlValue the new control value
\param paramInfo structure of information about why this value is being udpated (e.g as a result of a preset being loaded vs. the top of a buffer process cycle)

\return true if operation succeeds, false otherwise
*/
bool PluginCore::updatePluginParameter(int32_t controlID, double controlValue, ParameterUpdateInfo& paramInfo)
{
    // --- use base class helper
    setPIParamValue(controlID, controlValue);

    // --- do any post-processing
    postUpdatePluginParameter(controlID, controlValue, paramInfo);

    return true; /// handled
}

/**
\brief update the PluginParameter's value based on *normlaized* GUI control, preset, or data smoothing (thread-safe)

Operation:
- update the parameter's value (with smoothing this initiates another smoothing process)
- call postUpdatePluginParameter to do any further processing

\param controlID the control ID value of the parameter being updated
\param normalizedValue the new control value in normalized form
\param paramInfo structure of information about why this value is being udpated (e.g as a result of a preset being loaded vs. the top of a buffer process cycle)

\return true if operation succeeds, false otherwise
*/
bool PluginCore::updatePluginParameterNormalized(int32_t controlID, double normalizedValue, ParameterUpdateInfo& paramInfo)
{
	// --- use base class helper, returns actual value
	double controlValue = setPIParamValueNormalized(controlID, normalizedValue, paramInfo.applyTaper);

	// --- do any post-processing
	postUpdatePluginParameter(controlID, controlValue, paramInfo);

	return true; /// handled
}

/**
\brief perform any operations after the plugin parameter has been updated; this is one paradigm for
	   transferring control information into vital plugin variables or member objects. If you use this
	   method you can decode the control ID and then do any cooking that is needed. NOTE: do not
	   overwrite bound variables here - this is ONLY for any extra cooking that is required to convert
	   the GUI data to meaninful coefficients or other specific modifiers.

\param controlID the control ID value of the parameter being updated
\param controlValue the new control value
\param paramInfo structure of information about why this value is being udpated (e.g as a result of a preset being loaded vs. the top of a buffer process cycle)

\return true if operation succeeds, false otherwise
*/
bool PluginCore::postUpdatePluginParameter(int32_t controlID, double controlValue, ParameterUpdateInfo& paramInfo)
{
    // --- now do any post update cooking; be careful with VST Sample Accurate automation
    //     If enabled, then make sure the cooking functions are short and efficient otherwise disable it
    //     for the Parameter involved
    switch(controlID)
    {
     case controlID::delTime:
     {
         // Cook delay time in ms to samples
         cookedDelay = delTime*getSampleRate()/1000.0;
         return true;    ///handled
     }
     case controlID::modDepthL:
     {
         // Cook depth from % to linear
         cookedDepthL = modDepthL/100.0;
         return true;    /// handled
     }
    case controlID::modDepthR:
     {
         // Cook depth from % to linear
        cookedDepthR = modDepthR/100.0;
        return true;    /// handled
     }
     case controlID::modMix:
     {
         // Cook dry amount such that it never reaches zero so there's always chorusing
         dryAmount = pow(exp(-modMix/140),1.4);
     }
     case controlID::phaseOffset:
     {
         // Cook phase offset from angles to linear multiplier
         cookedPhaseOffset = phaseOffset/180.0;
     }
     case controlID::feedbackAmt:
     {
         // Cook feedback amount from % to linear, also curbing it a bit to avoid instability
         cookedFeedback = feedbackAmt/130.0;
     }
     case controlID::voiceCount:
     {
         // Convert enum # to voice count number
         voiceCount = pow(2,voiceCountSwitch) ;
     }
     case controlID::volume:
     {
         // Cook volume from dB to linear
         volumeLin = pow(10,volumedB/20);
     }
     case controlID::feedbackFilter:
     {
         // Set cutoff freq/Q for feedback HPF filter and calculate coeff
         feedbackFilter.setCutOffFreq(dzHPF.smooth(feedbackFilterCutoff));
         feedbackFilter.setQFactor(0.707100);
         feedbackFilter.calculateHPFCoeff();
     }
    case controlID::preDistortionFilter:
    {
         // Set cutoff freq/Q for pre distortion LPF filter and calculate coeff
         preDistortionFilter.setCutOffFreq(preDistortionFilterCutoff);
         preDistortionFilter.setQFactor(0.707100);
         preDistortionFilter.calculateLPFCoeff();
    }
     case controlID::preChorusDistortion:
     {
         // Cook overdrive to % to multipler - 0.1 to 10.1
         preChorusDistortion.setOverdrive((saturation/10) + 0.1);
     }
            
        default:return false;   /// not handled
    }

    return false;
}

/**
\brief has nothing to do with actual variable or updated variable (binding)

CAUTION:
- DO NOT update underlying variables here - this is only for sending GUI updates or letting you
  know that a parameter was changed; it should not change the state of your plugin.

WARNING:
- THIS IS NOT THE PREFERRED WAY TO LINK OR COMBINE CONTROLS TOGETHER. THE PROPER METHOD IS
  TO USE A CUSTOM SUB-CONTROLLER THAT IS PART OF THE GUI OBJECT AND CODE.
  SEE http://www.willpirkle.com for more information

\param controlID the control ID value of the parameter being updated
\param actualValue the new control value

\return true if operation succeeds, false otherwise
*/
bool PluginCore::guiParameterChanged(int32_t controlID, double actualValue)
{
	/*
	switch (controlID)
	{
		case controlID::<your control here>
		{

			return true; // handled
		}

		default:
			break;
	}*/

	return false; /// not handled
}

/**
\brief For Custom View and Custom Sub-Controller Operations

NOTES:
- this is for advanced users only to implement custom view and custom sub-controllers
- see the SDK for examples of use

\param messageInfo a structure containing information about the incoming message

\return true if operation succeeds, false otherwise
*/
bool PluginCore::processMessage(MessageInfo& messageInfo)
{
	// --- decode message
	switch (messageInfo.message)
	{
		// --- add customization appearance here
	case PLUGINGUI_DIDOPEN:
	{
		return false;
	}

	// --- NULL pointers so that we don't accidentally use them
	case PLUGINGUI_WILLCLOSE:
	{
		return false;
	}

	// --- update view; this will only be called if the GUI is actually open
	case PLUGINGUI_TIMERPING:
	{
		return false;
	}

	// --- register the custom view, grab the ICustomView interface
	case PLUGINGUI_REGISTER_CUSTOMVIEW:
	{

		return false;
	}

	case PLUGINGUI_REGISTER_SUBCONTROLLER:
	case PLUGINGUI_QUERY_HASUSERCUSTOM:
	case PLUGINGUI_USER_CUSTOMOPEN:
	case PLUGINGUI_USER_CUSTOMCLOSE:
	case PLUGINGUI_EXTERNAL_SET_NORMVALUE:
	case PLUGINGUI_EXTERNAL_SET_ACTUALVALUE:
	{

		return false;
	}

	default:
		break;
	}

	return false; /// not handled
}


/**
\brief process a MIDI event

NOTES:
- MIDI events are 100% sample accurate; this function will be called repeatedly for every MIDI message
- see the SDK for examples of use

\param event a structure containing the MIDI event data

\return true if operation succeeds, false otherwise
*/
bool PluginCore::processMIDIEvent(midiEvent& event)
{
	// --- IF PROCESSING AUDIO BLOCKS: push into vector for block processing
	if (!pluginDescriptor.processFrames)
	{
		processBlockInfo.pushMidiEvent(event);
		return true;
	}

	// --- IF PROCESSING AUDIO FRAMES: decode AND service this MIDI event here
	//     for sample accurate MIDI
	// myMIDIMessageHandler(event); // <-- you write this

	return true;
}

/**
\brief (for future use)

NOTES:
- MIDI events are 100% sample accurate; this function will be called repeatedly for every MIDI message
- see the SDK for examples of use

\param vectorJoysickData a structure containing joystick data

\return true if operation succeeds, false otherwise
*/
bool PluginCore::setVectorJoystickParameters(const VectorJoystickData& vectorJoysickData)
{
	return true;
}

/**
\brief create all of your plugin parameters here

\return true if parameters were created, false if they already existed
*/
bool PluginCore::initPluginParameters()
{
	if (pluginParameterMap.size() > 0)
		return false;

	// --- Add your plugin parameter instantiation code bewtween these hex codes
	// **--0xDEA7--**
    
    PluginParameter* piParam = nullptr;

    piParam = new PluginParameter(controlID::modRate, "Mod Rate", "Hz", controlVariableType::kDouble, 0.100000, 2.000000, 0.10000, taper::kLinearTaper);
    piParam->setBoundVariable(&modRate, boundVariableType::kDouble);
    addPluginParameter(piParam);

    piParam = new PluginParameter(controlID::modDepthL, "Mod Depth L", "%", controlVariableType::kDouble, 5.000000, 99.00000, 50.000000, taper::kLinearTaper);
    piParam->setBoundVariable(&modDepthL, boundVariableType::kDouble);
    addPluginParameter(piParam);

    piParam = new PluginParameter(controlID::delTime, "Delay Time", "ms", controlVariableType::kDouble, 5.000000, 60.000000, 30.000000, taper::kLinearTaper);
    piParam->setBoundVariable(&delTime, boundVariableType::kDouble);
    addPluginParameter(piParam);

    piParam = new PluginParameter(controlID::modMix, "Wet Mix", "%", controlVariableType::kDouble, 0.000000, 100.000000, 50.000000, taper::kLinearTaper);
    piParam->setBoundVariable(&modMix, boundVariableType::kDouble);
    addPluginParameter(piParam);

    piParam = new PluginParameter(controlID::phaseOffset, "Phase Offset", "°", controlVariableType::kDouble, 0.000000, 180.000000, 90.000000, taper::kLinearTaper);
    piParam->setBoundVariable(&phaseOffset, boundVariableType::kDouble);
    addPluginParameter(piParam);
    
    piParam = new PluginParameter(controlID::feedbackAmt, "Feedback Amt", "%", controlVariableType::kDouble, 0.000000, 100.000000, 0.000000, taper::kLinearTaper);
    piParam->setBoundVariable(&feedbackAmt, boundVariableType::kDouble);
    addPluginParameter(piParam);
    
    piParam = new PluginParameter(controlID::feedbackPolarity, "Feedback Polarity", "-,+", "+");
    piParam->setBoundVariable(&feedbackPolarity, boundVariableType::kInt);
    addPluginParameter(piParam);
    
    piParam = new PluginParameter(controlID::crossFeedback, "Feedback Type", "Series,Cross", "Series");
    piParam->setBoundVariable(&crossfeedback, boundVariableType::kInt);
    addPluginParameter(piParam);
    
    piParam = new PluginParameter(controlID::waveformType, "Waveform Type", "Sine,Asym", "Sine");
    piParam->setBoundVariable(&waveformType, boundVariableType::kInt);
    addPluginParameter(piParam);
    
    piParam = new PluginParameter(controlID::effectBypass, "Effect Bypass", "OFF,ON", "OFF");
    piParam->setBoundVariable(&effectBypass, boundVariableType::kInt);
    addPluginParameter(piParam);
    
    /* log RMS VU Meter Left*/
    piParam = new PluginParameter(controlID::vuL, "VU Left", 5.00, 250.00, ENVELOPE_DETECT_MODE_RMS, meterCal::kLogMeter);
    piParam->setInvertedMeter(false);
    piParam->setIsProtoolsGRMeter(false);
    piParam->setBoundVariable(&vuL, boundVariableType::kFloat);
    addPluginParameter(piParam);

    /* log RMS VU Meter Right*/
    piParam = new PluginParameter(controlID::vuR, "VU Right", 5.00, 250.00, ENVELOPE_DETECT_MODE_RMS, meterCal::kLogMeter);
    piParam->setInvertedMeter(false);
    piParam->setIsProtoolsGRMeter(false);
    piParam->setBoundVariable(&vuR, boundVariableType::kFloat);
    addPluginParameter(piParam);
    
    piParam = new PluginParameter(controlID::voiceCount, "Voices", "1,2,3,4", "8");
    piParam->setBoundVariable(&voiceCountSwitch, boundVariableType::kInt);
    addPluginParameter(piParam);
    
    piParam = new PluginParameter(controlID::volume, "Volume", "dB", controlVariableType::kDouble, -96.000000, 12.000000, 0.000000, taper::kLinearTaper);
    piParam->setBoundVariable(&volumedB, boundVariableType::kDouble);
    addPluginParameter(piParam);
    
    piParam = new PluginParameter(controlID::feedbackFilter, " Feedback HPF", "Hz", controlVariableType::kDouble, 200.000000, 20000.000000, 5000.000000, taper::kLinearTaper);
    piParam->setBoundVariable(&feedbackFilterCutoff, boundVariableType::kDouble);
    addPluginParameter(piParam);
    
    piParam = new PluginParameter(controlID::preChorusDistortion, "Saturation", "%", controlVariableType::kDouble, 0.00000, 100.000000, 0.000000, taper::kLinearTaper);
    piParam->setBoundVariable(&saturation, boundVariableType::kDouble);
    addPluginParameter(piParam);
    
    piParam = new PluginParameter(controlID::preDistortionFilter, "Pre Distortion LPF", "Hz", controlVariableType::kDouble, 250.000000, 20000.000000, 15000.000000, taper::kLinearTaper);
    piParam->setBoundVariable(&preDistortionFilterCutoff, boundVariableType::kDouble);
    addPluginParameter(piParam);
    
    piParam = new PluginParameter(controlID::modDepthR, "Mod Depth R", "%", controlVariableType::kDouble, 5.000000, 99.000000, 50.000000, taper::kLinearTaper);
    piParam->setBoundVariable(&modDepthR, boundVariableType::kDouble);
    addPluginParameter(piParam);

	// **--0xEDA5--**

	// --- BONUS Parameter
	// --- SCALE_GUI_SIZE
	PluginParameter* piParamBonus = new PluginParameter(SCALE_GUI_SIZE, "Scale GUI", "tiny,small,medium,normal,large,giant", "normal");
	addPluginParameter(piParamBonus);

	// --- create the super fast access array
	initPluginParameterArray();

	return true;
}

/**
\brief use this method to add new presets to the list

NOTES:
- see the SDK for examples of use
- for non RackAFX users that have large paramter counts, there is a secret GUI control you
  can enable to write C++ code into text files, one per preset. See the SDK or http://www.willpirkle.com for details

\return true if operation succeeds, false otherwise
*/
bool PluginCore::initPluginPresets()
{
	// **--0xFF7A--**

	// **--0xA7FF--**

    return true;
}

/**
\brief setup the plugin description strings, flags and codes; this is ordinarily done through the ASPiKreator or CMake

\return true if operation succeeds, false otherwise
*/
bool PluginCore::initPluginDescriptors()
{
	// --- setup audio procssing style
	//
	// --- kProcessFrames and kBlockSize are set in plugindescription.h
	//
	// --- true:  process audio frames --- less efficient, but easier to understand when starting out
	//     false: process audio blocks --- most efficient, but somewhat more complex code
	pluginDescriptor.processFrames = kProcessFrames;

	// --- for block processing (if pluginDescriptor.processFrame == false),
	//     this is the block size
	processBlockInfo.blockSize = kBlockSize;

    pluginDescriptor.pluginName = PluginCore::getPluginName();
    pluginDescriptor.shortPluginName = PluginCore::getShortPluginName();
    pluginDescriptor.vendorName = PluginCore::getVendorName();
    pluginDescriptor.pluginTypeCode = PluginCore::getPluginType();

	// --- describe the plugin attributes; set according to your needs
	pluginDescriptor.hasSidechain = kWantSidechain;
	pluginDescriptor.latencyInSamples = kLatencyInSamples;
	pluginDescriptor.tailTimeInMSec = kTailTimeMsec;
	pluginDescriptor.infiniteTailVST3 = kVSTInfiniteTail;

    // --- AAX
    apiSpecificInfo.aaxManufacturerID = kManufacturerID;
    apiSpecificInfo.aaxProductID = kAAXProductID;
    apiSpecificInfo.aaxBundleID = kAAXBundleID;  /* MacOS only: this MUST match the bundle identifier in your info.plist file */
    apiSpecificInfo.aaxEffectID = "aaxDeveloper.";
    apiSpecificInfo.aaxEffectID.append(PluginCore::getPluginName());
    apiSpecificInfo.aaxPluginCategoryCode = kAAXCategory;

    // --- AU
    apiSpecificInfo.auBundleID = kAUBundleID;   /* MacOS only: this MUST match the bundle identifier in your info.plist file */
    apiSpecificInfo.auBundleName = kAUBundleName;

    // --- VST3
    apiSpecificInfo.vst3FUID = PluginCore::getVSTFUID(); // OLE string format
    apiSpecificInfo.vst3BundleID = kVST3BundleID;/* MacOS only: this MUST match the bundle identifier in your info.plist file */
	apiSpecificInfo.enableVST3SampleAccurateAutomation = kVSTSAA;
	apiSpecificInfo.vst3SampleAccurateGranularity = kVST3SAAGranularity;

    // --- AU and AAX
    apiSpecificInfo.fourCharCode = PluginCore::getFourCharCode();

    return true;
}

// --- static functions required for VST3/AU only --------------------------------------------- //
const char* PluginCore::getPluginBundleName() { return getPluginDescBundleName(); }
const char* PluginCore::getPluginName(){ return kPluginName; }
const char* PluginCore::getShortPluginName(){ return kShortPluginName; }
const char* PluginCore::getVendorName(){ return kVendorName; }
const char* PluginCore::getVendorURL(){ return kVendorURL; }
const char* PluginCore::getVendorEmail(){ return kVendorEmail; }
const char* PluginCore::getAUCocoaViewFactoryName(){ return AU_COCOA_VIEWFACTORY_STRING; }
pluginType PluginCore::getPluginType(){ return kPluginType; }
const char* PluginCore::getVSTFUID(){ return kVSTFUID; }
int32_t PluginCore::getFourCharCode(){ return kFourCharCode; }



