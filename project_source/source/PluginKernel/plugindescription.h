// --- CMAKE generated variables for your plugin

#include "pluginstructures.h"

#ifndef _plugindescription_h
#define _plugindescription_h

#define QUOTE(name) #name
#define STR(macro) QUOTE(macro)
#define AU_COCOA_VIEWFACTORY_STRING STR(AU_COCOA_VIEWFACTORY_NAME)
#define AU_COCOA_VIEW_STRING STR(AU_COCOA_VIEW_NAME)

// --- AU Plugin Cocoa View Names (flat namespace) 
#define AU_COCOA_VIEWFACTORY_NAME AUCocoaViewFactory_BB7B0C3D2A2439DCA7AA04BCDAAEBAD3
#define AU_COCOA_VIEW_NAME AUCocoaView_BB7B0C3D2A2439DCA7AA04BCDAAEBAD3

// --- BUNDLE IDs (MacOS Only) 
const char* kAAXBundleID = "developer.aax.mychorusplugin.bundleID";
const char* kAUBundleID = "developer.au.mychorusplugin.bundleID";
const char* kVST3BundleID = "developer.vst3.mychorusplugin.bundleID";

// --- Plugin Names 
const char* kPluginName = "MyChorusPlugin";
const char* kShortPluginName = "MyChorusPlugin";
const char* kAUBundleName = "MyChorusPlugin_AU";
const char* kAAXBundleName = "MyChorusPlugin_AAX";
const char* kVSTBundleName = "MyChorusPlugin_VST";

// --- bundle name helper 
inline static const char* getPluginDescBundleName() 
{ 
#ifdef AUPLUGIN 
	return kAUBundleName; 
#endif 

#ifdef AAXPLUGIN 
	return kAAXBundleName; 
#endif 

#ifdef VSTPLUGIN 
	return kVSTBundleName; 
#endif 

	// --- should never get here 
	return kPluginName; 
} 

// --- Plugin Type 
const pluginType kPluginType = pluginType::kFXPlugin;

// --- VST3 UUID 
const char* kVSTFUID = "{bb7b0c3d-2a24-39dc-a7aa-04bcdaaebad3}";

// --- 4-char codes 
const int32_t kFourCharCode = 'ASE2';
const int32_t kAAXProductID = 'ASE2';
const int32_t kManufacturerID = 'APGV';

// --- Vendor information 
const char* kVendorName = "My Company";
const char* kVendorURL = "www.myplugins.com";
const char* kVendorEmail = "support@myplugins.com";

// --- Plugin Options 
const bool kProcessFrames = true;
const uint32_t kBlockSize = DEFAULT_AUDIO_BLOCK_SIZE;
const bool kWantSidechain = false;
const uint32_t kLatencyInSamples = 0;
const double kTailTimeMsec = 0;
const bool kVSTInfiniteTail = false;
const bool kVSTSAA = false;
const uint32_t kVST3SAAGranularity = 1;
const uint32_t kAAXCategory = 0;

#endif
